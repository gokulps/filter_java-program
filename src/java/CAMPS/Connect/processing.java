/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CAMPS.Connect;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.*;
import java.util.Date;
import java.security.*;

@WebServlet(name = "processing", urlPatterns = {"/processing"})
public class processing extends HttpServlet {

    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            DBConnect conn = new DBConnect();
            int flag=0;
            try{
            conn.getConnection();
            String name1 = request.getParameter("Field1");
            String password1 = request.getParameter("Field2");
            mdjavahash md = new mdjavahash();
            String mypass = md.getHashPass(password1);
            escapeSpecialChars objesc = new escapeSpecialChars();
            conn.read("select * from campusdemo.demo where Name= '"+objesc.escapeSpecialChar(name1)+"' and password ='"+mypass+"'");
            
            while(conn.rs.next())
            {                 
                 
                     if(conn.rs.getInt("status")==0)
                     {
                        //out.print("alert('User Deleted Contact Admin for more info!!!');");
                         
                        RequestDispatcher rd = request.getRequestDispatcher("Authentication Required.jsp");
                        rd.forward(request,response);
                        flag=0;
                        break;
                     }
                 Date dbnow = new Date();                 
                 HttpSession session = request.getSession();
                 session.setAttribute("U_NAME",name1);
                 session.setAttribute("Time",dbnow.toString());
                 RequestDispatcher rd = request.getRequestDispatcher("Login_Page.jsp");
                 rd.forward(request,response);
                     
                 flag=1;
                 
            }
            if(flag==0)
            {
                HttpSession session = request.getSession();
                String n = (String)session.getAttribute("U_NAME");
                if(n!=null)
                {
                 RequestDispatcher rd = request.getRequestDispatcher("Login_Page.jsp");
                 rd.forward(request,response);   
                }
                else 
                {
                    
                    int portaddr = request.getRemotePort();
                    String ip = request.getHeader("X-FORWARDED-FOR");
                    request.getHeader("VIA");
                    String ipAddress = request.getHeader("X-FORWARDED-FOR");
                    if (ipAddress == null) {
                         ipAddress = request.getRemoteAddr();
                       }
                    ip = ipAddress;
                    Date dbnow = new Date();
                    String now = dbnow.toString();
                    conn.insert("insert into campusdemo.track_view_wrong_user(port,name,password,time,ip) values("+portaddr+",'"+name1+"','"+password1+"','"+now+"','"+ip+"')");
                    out.print("Wrong UserName or Password");
                    RequestDispatcher rd = request.getRequestDispatcher("Authentication Required.jsp");
                    rd.forward(request,response);
                }
                /*out.print("Wrong UserName or Password");
                RequestDispatcher rd = request.getRequestDispatcher("Authentication Required.jsp");
                rd.forward(request,response);*/
            }
            }
            catch(Exception e)
            {
                out.print(e);
            }
            
        }
    }

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
