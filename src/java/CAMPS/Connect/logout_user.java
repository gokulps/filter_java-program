/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CAMPS.Connect;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpSession;

/**
 *
 * @author dell
 */
@WebServlet(name = "logout_user", urlPatterns = {"/logout_user"})
public class logout_user extends HttpServlet {

    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            DBConnect conn = new DBConnect();
            try
            {
                conn.getConnection();
                HttpSession session1 = request.getSession(false);
                Date dbcurr = new Date();
                String curr = dbcurr.toString();
                String timeold =(String)session1.getAttribute("Time");
                String n = (String)session1.getAttribute("U_NAME");
                if(timeold!=null&&n!=null){
                  String ip = request.getHeader("X-FORWARDED-FOR");
                request.getHeader("VIA");
                String ipAddress = request.getHeader("X-FORWARDED-FOR");
                    if (ipAddress == null) {
                         ipAddress = request.getRemoteAddr();
                       }
                ip = ipAddress;
                escapeSpecialChars objesc = new escapeSpecialChars();
                conn.insert("insert into campusdemo.track_view_correct_user(user_name,ip,login_time,logout_time)values('"+objesc.escapeSpecialChar(n)+"','"+ip+"','"+timeold+"','"+curr+"')");
                    
                }
                session1.invalidate();
                RequestDispatcher rd = request.getRequestDispatcher("Authentication Required.jsp");
                rd.forward(request,response);
                
            }
            catch(Exception e)
            {
                System.out.println(e);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
