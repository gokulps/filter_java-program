/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CAMPS.Connect;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.*;
import java.util.Date;
import javax.servlet.RequestDispatcher;
/**
 *
 * @author dell
 */
@WebServlet(name = "SignUp", urlPatterns = {"/SignUp"})
public class SignUp extends HttpServlet {

    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            DBConnect conn = new DBConnect();
            try
            {
                conn.getConnection();
                String name1 = request.getParameter("Field1");
                String password1 = request.getParameter("Field2");
                String password2 = request.getParameter("Field3");
                if(password1.equals(password2))
                {
                    mdjavahash md = new mdjavahash();
                    String mypass = md.getHashPass(password1);
                    String salt = password1.substring(2);
                    String mysalt = md.getHashPass(salt);
                    boolean flag=true;
                    escapeSpecialChars objesc = new escapeSpecialChars();
                    conn.read("select * from campusdemo.demo where Name = '"+objesc.escapeSpecialChar(name1)+"' and password = '"+mypass+"'");
                    while(conn.rs.next())
                    {
                        flag=false;
                    }
                    if(flag==true)
                    {
                    
                    int status=1;
                    String mypass1 = md.getHashPass(password1);
                    String ip = request.getHeader("X-FORWARDED-FOR");
                    request.getHeader("VIA");
                    String ipAddress = request.getHeader("X-FORWARDED-FOR");
                    if (ipAddress == null) {
                         ipAddress = request.getRemoteAddr();
                       }
                    ip = ipAddress;
                    Date dbnow = new Date();
                    String now = dbnow.toString();
                    conn.insert("insert into campusdemo.demo(Name,password,status,Psalt) values('"+objesc.escapeSpecialChar(name1)+"','"+mypass1+"','"+status+"','"+mysalt+"')");
                    conn.insert("insert into campusdemo.dbbirth_log(user_name,ip,date) values('"+objesc.escapeSpecialChar(name1)+"','"+ip+"','"+now+"')");
                    out.print("<h3>User Creation Successfull!!!</h3>");
                    }
                    else
                    {
                        out.print("<h3>User Already Existed</h3>");
                    }
                    
                }
                else
                {
                    out.print("<hr>Password Mismatch</hr>");
                    RequestDispatcher rd = request.getRequestDispatcher("Authentication Required.jsp");
                    rd.forward(request,response);
                }
                
                
            }
            catch(Exception e)
            {
                System.out.println(e);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
