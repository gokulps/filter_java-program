<%-- 
    Document   : logout
    Created on : 18 Aug, 2018, 11:38:39 PM
    Author     : dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Logged Out</title>
    </head>
    <body>
        <%
            
            RequestDispatcher rd = request.getRequestDispatcher("logout_user");
            rd.forward(request,response);
        %>
        <h1>Logout successful!!!</h1>
        
    </body>
</html>
